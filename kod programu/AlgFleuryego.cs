﻿using System;
using System.Collections.Generic;
using System.IO;

namespace program_do_Floyda_Warshalla
{
    class AlgFleuryego
    {

        List<List<int>> macierzSasiedztwa;
        List<int> listaKolejnoOdwiedzanychWierzcholkow;
        List<int> tablicaStopni;

        int liczbaWierzcholkow, liczbaKrawedzi, numeryWierzcholkow, liczbaWierzcholkowNaStosie;
        private string wejscie;
        int i, v1;
        List<int> listaPrzechowywujacaStopnieWierzcholkow;

        public AlgFleuryego()
        {
            Console.WriteLine("Podaj nazwę pliku wejściowego: ");
            wejscie = Console.ReadLine();
            if (!File.Exists(wejscie + ".txt"))
            {
                Console.Clear();
                Console.WriteLine("Plik " + wejscie + ".txt" + " nie istnieje.");
            }
            else
            {
                Console.Clear();
                string[] tablicaStringow;
                StreamReader sR = new StreamReader(wejscie + ".txt");
                string linia = sR.ReadLine();
                tablicaStringow = linia.Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
                liczbaWierzcholkow = int.Parse(tablicaStringow[0]);
                liczbaKrawedzi = int.Parse(tablicaStringow[1]);
                macierzSasiedztwa = PobieraczGrafu.zczytajZPliku(wejscie);
                listaPrzechowywujacaStopnieWierzcholkow = new List<int>();
                tablicaStopni = new List<int>();
                listaKolejnoOdwiedzanychWierzcholkow = new List<int>();

                for (int i = 0; i < liczbaWierzcholkow; i++)
                {
                    listaPrzechowywujacaStopnieWierzcholkow.Add(0);
                    tablicaStopni.Add(0);
                }
                Console.Write("Tablica stopni zainicjowana zerami: ");
                foreach (var item in tablicaStopni)
                {
                    Console.Write(item + " ");
                }
                Console.WriteLine();
                liczbaWierzcholkowNaStosie = 0;
                for (int i = 0; i < liczbaWierzcholkow; i++)
                    for (int j = 0; j < liczbaWierzcholkow; j++)
                        if (macierzSasiedztwa[i][j] == 1)
                        {
                            listaPrzechowywujacaStopnieWierzcholkow[i]++;
                            listaPrzechowywujacaStopnieWierzcholkow[j]++;
                        }
                for (v1 = 0; v1 < liczbaWierzcholkow; v1++)
                    if (listaPrzechowywujacaStopnieWierzcholkow[v1] != 0) break;

                for (i = v1; i < liczbaWierzcholkow; i++)
                    if (listaPrzechowywujacaStopnieWierzcholkow[i] % 2 != 0)
                    {
                        v1 = i;
                        break;
                    }

                znajdzCyklEulera(v1);

                if (listaPrzechowywujacaStopnieWierzcholkow[v1] % 2 == 0 && liczbaWierzcholkowNaStosie == liczbaKrawedzi + 1 && listaKolejnoOdwiedzanychWierzcholkow[0] == listaKolejnoOdwiedzanychWierzcholkow[liczbaWierzcholkowNaStosie - 1])
                {
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.WriteLine("Występuje cykl Eulera : ");
                    for (i = 0; i < liczbaWierzcholkowNaStosie; i++)
                        Console.Write(listaKolejnoOdwiedzanychWierzcholkow[i] + "   ");
                }
                else
                    Console.WriteLine("Cykl eulera nie występuje, przy założeniu że droga musi być zamknięta, a przez poszczególne krawędzie przechodzimy tylko jeden raz.");
                Console.ForegroundColor = ConsoleColor.White;
            }
        }


        public int DFSb(int v, int vf)
        {
            Console.ForegroundColor = ConsoleColor.Yellow;
            Console.WriteLine("DFSb(" + v + "," + vf + ")");
            Console.ForegroundColor = ConsoleColor.White;
            int l, temp, i;

            tablicaStopni[v] = numeryWierzcholkow;            
            l = numeryWierzcholkow;                   
            numeryWierzcholkow++;                        
            for (i = 0; i < liczbaWierzcholkow; i++)     
                if (macierzSasiedztwa[v][i] != 0 && (i != vf))
                {
                    if (tablicaStopni[i] == 0)              
                    {
                        temp = DFSb(i, v);
                        if (temp < l) l = temp; 
                    }
                    else if (tablicaStopni[i] < l) l = tablicaStopni[i];
                }

            if ((vf > -1) && (l == tablicaStopni[v])) // odnaleziono most
                macierzSasiedztwa[vf][v] = macierzSasiedztwa[v][vf] = 2;
            Console.ForegroundColor = ConsoleColor.Yellow;
            Console.WriteLine("DFSb zwraca wartość: " + l);
            Console.ForegroundColor = ConsoleColor.White;
            return l;
            
        }

        void znajdzCyklEulera(int v)
        {
            int u, w, i;
            listaKolejnoOdwiedzanychWierzcholkow = new List<int>();
            for (;;)
            {
                listaKolejnoOdwiedzanychWierzcholkow.Add(v); // dodajemy kolejno odwiedzane wierzchołki
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("Dodano odwiedzony wierzchołek nr " + v);
                Console.ForegroundColor = ConsoleColor.White;
                liczbaWierzcholkowNaStosie++;
                for (u = 0; (u < liczbaWierzcholkow) && macierzSasiedztwa[v][u] == 0; u++) ; // odnajdujemy pierwszego sąsiada v
                if (u == liczbaWierzcholkow)
                    break;
                for (i = 0; i < liczbaWierzcholkow; i++)
                    tablicaStopni[i] = 0;
                numeryWierzcholkow = 1;
                DFSb(v, -1);                // identykikacja mostów
                for (w = u + 1; (macierzSasiedztwa[v][u] == 2) && (w < liczbaWierzcholkow); w++)
                    if (macierzSasiedztwa[v][w] != 0) u = w;
                macierzSasiedztwa[v][u] = macierzSasiedztwa[u][v] = 0;
                v = u;
            }
        }
    }
}

