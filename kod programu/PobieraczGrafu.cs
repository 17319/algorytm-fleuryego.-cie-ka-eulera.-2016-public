﻿using System;
using System.Collections.Generic;
using System.IO;

namespace program_do_Floyda_Warshalla
{
    class PobieraczGrafu
    {

        public static List<List<int>> zczytajZPliku(string nazwaPliku)
        {
            try
            {
                List<List<int>> macierzSasiedztwa = new List<List<int>>();
                string[] tablicaStringow;
                StreamReader sR = new StreamReader(nazwaPliku + ".txt");
                string linia = sR.ReadLine();
                tablicaStringow = linia.Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);

                int v = int.Parse(tablicaStringow[0]);
                int e = int.Parse(tablicaStringow[1]);

                for (int i = 0; i < v; i++)
                {
                    macierzSasiedztwa.Add(new List<int>());
                    for (int j = 0; j < v; j++)
                        macierzSasiedztwa[i].Add(0);
                }
                wypiszMacierze(macierzSasiedztwa, v);

                for (int i = 0; i < e; i++)
                {
                    linia = sR.ReadLine();
                    tablicaStringow = linia.Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
                    int x = int.Parse(tablicaStringow[0]);
                    int y = int.Parse(tablicaStringow[1]);
                    macierzSasiedztwa[x][y] = 1;
                    macierzSasiedztwa[y][x] = 1;
                    Console.WriteLine("Dodajemy: " + x + " -> " + y + "  Krawędź " + (i + 1));
                    Console.WriteLine("Dodajemy: " + y + " -> " + x + "  Krawędź " + (i + 1));
                    wypiszMacierze(macierzSasiedztwa, v);
                }
                List<List<List<int>>> listaDP = new List<List<List<int>>>();
                return macierzSasiedztwa;
            }
            catch (Exception)
            {
                Console.WriteLine("Nie zczytano danych z pliku");
                return new List<List<int>>();
            }
        }

        public static void wypiszMacierze(List<List<int>> d, int m)
        {
            Console.WriteLine("Macierz kosztów dojścia: ");
            Console.ForegroundColor = ConsoleColor.Red;
            Console.Write("d" + "\t");
            for (int i = 0; i < m; i++)
                Console.Write(i + "\t");
            for (int i = 0; i < m; i++)
            {
                Console.ForegroundColor = ConsoleColor.White;
                Console.WriteLine();
                for (int j = 0; j < m; j++)
                {
                    if (j == 0)
                    {
                        Console.ForegroundColor = ConsoleColor.Red;
                        Console.Write(i);
                        Console.ForegroundColor = ConsoleColor.White;
                        Console.Write("\t" + d[i][j] + "\t");
                    }
                    else
                        Console.Write(d[i][j] + "\t");
                }
            }

            Console.WriteLine();
            Console.WriteLine("==========================================================================");
            Console.WriteLine("==========================================================================");
        }

    }
}
